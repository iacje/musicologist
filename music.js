const musicInfo = [];

function addSongFromField(event) {
  event.preventDefault();
  $(this).blur();

  const info = $('#musicField').eq(0).val();
  if (info.length > 0) {
    musicInfo.push(info);
    renderList();
    $('#musicField').eq(0).val('');
  }
}

$('#addButton').click(addSongFromField);
$('#musicField').keyup(function(event) {
  if (event.which == 13) { // User presses Enter
    addSongFromField(event);
  }
});

function renderList() {
  const $list = $('.info').eq(0);

  $list.empty();

  for (const info of musicInfo) {

    const $item = $('<li class="list-group-item">').text(info);
    const $button = $('<button type="button" class="close" aria-label="Close">').html("&times");

    $button.click(function(event) {
      $item.remove();
      const index = musicInfo.indexOf(info);
      if (index > -1) musicInfo.splice(index, 1);
    });

    $item.append($button);
    $list.append($item);
  }
}

$('#getPlaylistBtn').click(function (event) {
  // TODO: Display a list of music.
  // You may use anything from musicInfo.
  $(this).blur();
  renderPlayList();
});

var numSongs = 1;

function renderPlayList() {
  const $title = $('.playlist-title').eq(0);
  if ($title.children().length == 0) {
    $h2 = $('<h2>');
    $h2.append($('<span class="glyphicon glyphicon-music" aria-hidden="true">'));
    $title.append($h2.append($('<span>').text(" My Recommended Playlist")));
    $('.table').css("visibility", "visible");
  }

  const $playlist = $('.playlist').eq(0);
  $playlist.empty();
  numSongs = 1;
  let allSongs = [];
  for (const info of musicInfo) {
    // https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/#searching
    $.get("https://itunes.apple.com/search?media=music&entity=song&limit=3&term=" + info).then(function(songs) {
      for (const song of JSON.parse(songs).results) {
        const concatSong = song.trackCensoredName + song.artistName;
        if (allSongs.indexOf(concatSong) < 0) {
          allSongs.push(concatSong);
          displaySong(song);
        }
      }
    })
    .catch( function() {
      alert("Unable to recommend a playlist at this time. Please try again later!" );
    });
  }
}

$.fn.playPause = function playPause() {
  $(this).children().first().toggleClass("glyphicon-play glyphicon-pause");
  $(this).toggleClass("play pause");
};

function displaySong(song) {
  const $playlist = $('.playlist').eq(0);
  const $tr = $('<tr>')
  
  const $number = $('<td class="text-center number">');
  $number.append($('<p>').text(numSongs));
  numSongs += 1;

  const $artwork = $('<td>');
  $artwork.append($('<img class="img-responsive center-block">').attr("src", song.artworkUrl60));

  const $content = $('<td>');
  $content.append($('<p>').append($('<a class="trackName">').attr("href", song.trackViewUrl).text(song.trackCensoredName)));
  $content.append($('<a>').attr("href", song.artistViewUrl).text(song.artistName));
 
  const $timePlay = $('<td>');
  $timePlay.append($('<p class="time">').text(millisToMinsAndSecs(song.trackTimeMillis)));
  const $playButton = $('<button class="btn btn-default play btn-circle">');
  $playButton.append($('<span class="glyphicon glyphicon-play play-button" aria-hidden="true">'));
  const $music = $('<audio>').attr("src", song.previewUrl);
  $timePlay.append($playButton).append($music);

  $tr.append($number).append($artwork).append($content).append($timePlay);
  $playlist.append($tr);

  $playButton.click(function(event) {
    $(this).blur();
    const music = $music.get(0);
    // https://www.w3.org/wiki/HTML/Elements/audio
    if (music.paused) {
      $('audio').each(function() {
        if (!($(this).get(0).paused)) {
          $(this).prev().playPause();
          $(this).get(0).pause();
        }
      });
      music.play();
      $('.number').css("color", "white");
      $music.parent().siblings(':first').css("color", "#0066ff");
    } else {
      music.pause();
      $music.parent().siblings(':first').css("color", "white");
    }
    $(this).playPause();
  });

  $music.on("ended", function() {
    $playButton.playPause();
  });
}

// https://stackoverflow.com/questions/21294302/converting-milliseconds-to-minutes-and-seconds-with-javascript
function millisToMinsAndSecs(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = Math.floor(((millis % 60000) / 1000)).toFixed(0);
  return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}
